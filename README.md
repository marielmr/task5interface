Task5Interface

Framework 4.5

We chose Net Framework instead of Net Core because we think that, according to the project requirements, we not need all the power or functionalities of Net Core. On the other hand, we all mostly 
know Net Framework so we feel more confident working with that framework. Finally, we are working with Net 4.5 because OpenCVSharp4 only need a Net Framework 4.0 or earlier.