﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace InterfaceImageOperators
{
    public interface IImageOperators
    {

		//Specification reference: 2.7.1
		//Book reference: 2.1,2.2
		/// <summary>
		/// Clears or sets an image to black color (to zero)
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat ClearImage(Mat img);

		//Specification reference: 2.7.1
		//Book reference: 2.1,2.2
		/// <summary>
		/// Clears or sets an image to a constant value.
		/// <para>
		/// This function sets an image to an especific intensity.
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		///<param name="value">Pixel value</param>
		Mat ClearImage(Mat img, float value);

		//Specification reference: 2.7.2
		//Book reference: 2.3
		/// <summary>
		/// Returns a copy of an Image.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat CopyImage(Mat img);

		//Specification reference: 2.7.3
		//Book reference: 2.4 
		/// <summary>
		/// Returns a inverted Image.
		/// <para>
		/// Inverts the intensity of all pixel in an image
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat InvertImage(Mat img);

		//Specification reference: 2.7.4
		//Book reference: 2.5,2.6
		/// <summary>
		/// Returns a translated Image from origin.
		/// <para>
		/// Translates an image in 8 directions( top, bottom, right, left, top-right, top-left, bottom-right,bottom-left)
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="direction">Direction of translation</param>
		/// <param name="steps">Number of pixels to translate</param>
		/// <param name="optionalSteps">Number of pixels to translate if corners of the image</param>
		Mat ShiftImage(Mat img, DirectionsEnum direction,float steps, float optionalSteps = 0);

		//Specification reference: 2.7.4
		//Book reference: 2.5,2.6
		/// <summary>
		/// Returns a translated Image from origin.
		/// <para>
		/// Translates an image a number of steps in X and Y direction
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="xSteps">Number of steps in X coordinate</param>
		/// <param name="ySteps">Number of steps in Y coordinate</param>
		Mat ShiftImage(Mat img, float xSteps, float ySteps);

		//Specification reference: 2.7.4
		//Book reference: 2.5,2.6
		/// <summary>
		/// Returns a translated Image from origin.
		/// <para>
		/// Translates an image with the matrix specified
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="translationMatrix">Matrix for translation</param>
		Mat ShiftImage(Mat img, Mat translationMatrix);

		//Specification reference: 2.7.5
		//Book reference: 2.7
		/// <summary>
		///  Adds a constant value to all pixels of an image.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="constantValue">Constant value to add</param>
		Mat AddConstant(Mat img, float constantValue);

		//Specification reference: 2.7.6
		//Book reference: 2.8,2,9
		/// <summary>
		/// Scales the pixel intensity of an Image.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="gamma">Multiplicator value</param>
		/// <param name="beta">Offset value</param>
		Mat Scaling(Mat img,float gamma, float beta);

		//Specification reference: 2.7.7
		//Book reference: 2.10
		/// <summary>
		/// Returns a binary thresholded mat object.
		/// <para>
		/// Threshold an image to convert it to binary image.
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="threshold">Threshold value</param>
		Mat ThresholdImage(Mat img, float threshold);

		//Specification reference: 2.7.8
		/// <summary>
		/// Returns a binary thresholded mat object 
		/// <para>
		/// Threshold an image to convert it to binary image, with high and low threshold limits.
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="lowerThreshold">Lower threshold limit</param>
		/// <param name="upperThreshold">Upper threshold limit</param>
		Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold);

		//Specification reference: 2.7.9
		/// <summary>
		/// Returns a binary thresholded mat object.
		/// <para>
		/// Threshold an image to convert it to binary image, with high and low threshold limits and an option to invert it.
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="lowerThreshold">lower threshold limit</param>
		/// <param name="upperThreshold">Upper threshold limit</param>
		/// <param name="invertedThresh">Invert or not the threshold </param>
		Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh);

		//Specification reference: 2.7.10
		/// <summary>
		/// Returns a binary thresholded mat object.
	 	/// <para>
		/// Threshold an image to convert it to binary image, with high and low threshold limits and an option to invert it.
		/// </para>
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="lowerThreshold">Lower threshold limit</param>
		/// <param name="upperThreshold">Upper threshold limit</param>
		/// <param name="invertedThresh">Invert or not the threshold </param>
		/// <param name="keepVal">Background intensity in the thresholded image</param>
		Mat ThresholdImage(Mat img, int lowerThreshold, int upperThreshold, bool invertedThresh, float keepVal);

		//Specification reference: 2.7.11
		//Book reference: 2.10
		/// <summary>
		/// Stretches the contrast of the input binary image. 
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat VisualiseBinary(Mat img);

		//Specification reference: 2.7.12
		//Book reference: 2.14,2.15
		/// <summary>
		/// Shrinks a binary object.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat Erosion(Mat img);

		//Specification reference: 2.7.13
		//Book reference: 2.16
		/// <summary>
		/// Expands a binary object.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat Dilation(Mat img);


		//Specification reference: 2.7.14
		//Book reference: 2.17
		/// <summary>
		/// Finds the boundary in a binary image.   
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat BinaryBoundary(Mat img);

		//Specification reference: 2.7.15
		//Book reference: 2.18
		/// <summary>
		/// Removes salt noise in binary image.   
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat RemoveSaltNoise(Mat img);


		//Specification reference: 2.7.16
		//Book reference: 2.19
		/// <summary>
		/// Removes pepper noise in binary image.   
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat RemovePepperNoise(Mat img);

		//Specification reference: 2.7.17
		//Book reference: 2.20
		/// <summary>
		/// Removes both salt and pepper noise in binary image   
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat RemoveNoise(Mat img);

		//Specification reference: 2.7.18
		//Book reference: 2.21
		/// <summary>
		/// Removes noise in a less stringent way in binary image.   
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		Mat RemoveNoiseSpurs(Mat img);

		//Specification reference: 2.7.19
		//Book reference: 2.22-2.26
		/// <summary>
		/// Applies basic convulation mask to an Image.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="convolutionMask">Convolution matrix </param>
		Mat BasicConvolutionImage(Mat img, Mat convolutionMask);

		//Specification reference: 1
		//Book reference: 2.7.1
		/// <summary>
		///	Difference between a mat original and mat obtained from Erosion own method.
		/// </summary>
		/// <param name="img">N-dimensional dense array class</param>
		/// <param name="erosionImg">N-dimensional dense array class of Erosion own method</param>
		/// <returns></returns>
		Mat DiffBinary(Mat img, Mat erosionImg);
	}
}
