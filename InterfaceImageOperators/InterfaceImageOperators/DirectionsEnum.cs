﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceImageOperators
{
    public enum DirectionsEnum
    {
        Top,
        Bottom, 
        Right, 
        Left, 
        TopRight, 
        TopLeft, 
        BottomRight, 
        BottomLeft

    }
}
